module.exports = function (url, options) {
    return fetch(url, options).then(response => {
        if (response.status === 401) {
            response.json().then(({redirect}) => {
                document.location.assign(redirect);
            })
        } else if (response.status === 200) {
            return response;
        }
    })
};