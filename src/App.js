import React, {Component} from 'react';
import 'antd/dist/antd.css';
import './index.css';

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";


import WrappedNormalLoginForm from "./news/Login";
import Main from "./news/Main";


class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/login" component={WrappedNormalLoginForm}/>
                    <Route path="*" component={Main}/>
                </Switch>
            </Router>
        );
    }
}

export default App;
