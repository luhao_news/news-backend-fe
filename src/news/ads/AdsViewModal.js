import {Component} from "react";
import {Modal, Form} from "antd";
import React from "react";

export default Form.create({name: 'view_ads'})(
    class AdsViewModal extends Component {
        state = {

        };


        componentWillReceiveProps(nextProps, nextContext) {
            // setFieldsValue()
        }


        init = () => {
            let fields = this.props.fields;
            const {setFieldsValue} = this.props.form;
            setFieldsValue('name', fields.name)
        };

        render() {
            const {
                visible, onCancel
            } = this.props;

            const formItemLayout = {
                labelCol: {span: 6},
                wrapperCol: {span: 14},
            };
            return (
                <Modal
                    // mask={false}
                    title="查看新闻"
                    visible={visible}
                    okText="确定"
                    cancelButtonProps={{style: {display: 'none'}}}
                    onCancel={onCancel}
                    onOk={onCancel}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="广告名称"
                        >
                            <span className="ant-form-text">{this.props.fields.name}</span>
                        </Form.Item>
                        <Form.Item
                          label="导流链接"
                        >
                            <span className="ant-form-text">{`http://119.3.210.63:8090/api/ads?adsId=${this.props.fields.id}`}</span>
                        </Form.Item>
                        <Form.Item
                            label="广告链接"
                        >
                            <span className="ant-form-text">{this.props.fields.link}</span>
                        </Form.Item>
                        <Form.Item
                            label="新闻类型"
                            hasFeedback
                        >
                            <span className="ant-form-text">{this.props.fields.type}</span>
                        </Form.Item>

                        <Form.Item
                            label="图片链接"
                            style={{display: this.props.fields.type === 'banner' ? 'block' : 'none'}}
                        >
                            <span className="ant-form-text">{this.props.fields.imgLink}</span>
                        </Form.Item>
                        <Form.Item
                            label="热词权重"
                            style={{display: this.props.fields.type === 'hotWord' ? 'block' : 'none'}}
                        >
                            <span className="ant-form-text">{this.props.fields.weight}</span>
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
    }
);
