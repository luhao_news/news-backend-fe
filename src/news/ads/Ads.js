import React, {Component} from 'react';
import moment from 'moment'

import 'antd/dist/antd.css';
import {
    Table, Button, Input, Row, Col, message, Modal
} from 'antd';

import fetch from '../../util/fetch'

import reqwest from 'reqwest';

import AdsModal from './AdsModal'
import AdsViewModal from './AdsViewModal'
import Cookies from "js-cookie";

const confirm = Modal.confirm;


const Search = Input.Search;


class Ads extends Component {
    state = {
        data: [],
        pagination: {},
        loading: false,
        addVisible: false,
        viewVisible: false,
        confirmLoading: false,
        keyword: "",
        type: null,
        sort: null,
        filters: null,
        fields: {}
    };

    componentDidMount() {
        if (Cookies.get('user_type') !== 'admin') {
            this.props.history.push('/dashboard')
        } else {
            this.fetch({
                limit: 10,
                offset: 0
            });
        }
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.offset = (pagination.current - 1) * pagination.pageSize;
        this.setState({
            pagination: pager,
            filters,
            sorter,
        });

        if (filters.type) {
            filters.type = filters.type[0]
        }
        let query = {
            limit: pager.pageSize,
            offset: pager.offset,
            sort: (sorter.field && sorter.order) ? `${sorter.field}:${sorter.order}` : null,
            ...filters,
        };
        this.fetch(query);
    };

    fetch = (params = {}) => {
        this.setState({loading: true});

        fetch('/api/ads/list', {
            method: 'post',
            body: JSON.stringify(params),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            const pagination = {...this.state.pagination};
            pagination.total = data.data.count;
            this.setState({
                loading: false,
                data: data.data.result,
                pagination,
            });
        }).catch(err => {
            message.error('获取数据失败');
        });

        // reqwest({
        //     url: '/api/ads/list',
        //     method: 'post',
        //     cors: true,
        //     contentType: 'application/json',
        //     data: JSON.stringify(params),
        //     json: true
        // }).then((data) => {
        //     const pagination = {...this.state.pagination};
        //     pagination.total = data.data.count;
        //     this.setState({
        //         loading: false,
        //         data: data.data.result,
        //         pagination,
        //     });
        // });
    };

    add = () => {
        this.setState({
            fields: {
                from: 'add'
            },
            addVisible: true
        });
    };

    edit = (value) => {
        this.setState({
            fields: {...value, from: 'edit'},
            addVisible: true,
        });
    };

    view = (value) => {
        this.setState({
            fields: {...value},
            viewVisible: true,
        });
    };

    del = (value) => {
        let that = this;
        confirm({
            title: '确定删除该广告?',
            cancelText: '取消',
            okText: '确定',
            onOk() {
                var data = new URLSearchParams();
                data.append('id', value.id);
                fetch('/api/ads/delete', {
                    method: 'post',
                    body: data.toString(),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                }).then(response => {
                    message.success('删除成功');
                    that.fetch({
                        limit: 10,
                        offset: 0
                    });
                }).catch(err => {
                    message.error('删除失败');
                });
            },
            onCancel() {},
        });
    };

    publish = (value) => {
        var data = new URLSearchParams();
        data.append('id', value.id);
        fetch('/api/ads/publish', {
            method: 'post',
            body: data.toString(),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            },
        }).then(response => {
            if (value.publish === false) {
                message.success('发布成功');
            } else {
                message.success('取消发布成功');
            }
            const newData = [...this.state.data];
            const index = newData.findIndex(item => value.id === item.id);
            const item = newData[index];
            item.publish = !item.publish;
            this.setState({ data: newData});
        }).catch(err => {
            if (value.publish === false) {
                message.success('发布失败');
            } else {
                message.error('取消发布失败');
            }
        });
    };

    render() {
        let {addVisible, viewVisible, confirmLoading} = this.state;

        const columns = [{
            title: '日期',
            dataIndex: 'createTime',
            sorter: true,
            sortDirections: ['asc', 'desc'],
            width: '20%',
            render: (value) => {
                return moment(value).format('YYYY-MM-DD HH:mm:ss')
            }
        }, {
            title: '广告名',
            dataIndex: 'name',
            width: '20%',
        }, {
            title: '广告类型',
            dataIndex: 'type',
            filters: [
                {text: 'banner', value: 'banner'},
                {text: '搜索热词', value: 'hotWord'},
            ],
            filteredValue: this.state.filterType,
            filterMultiple: false,
            width: '10%',
        }, {
            title: '点击次数',
            dataIndex: 'times'
        }, {
            title: '发布',
            dataIndex: 'publish',
            filterMultiple: false,
            filters: [
                {text: '已发布', value: true},
                {text: '未发布', value: false},
            ],
            render: value => {
                return (
                    <div>{value ? '已发布' : '未发布'}</div>
                )
            }
        }, {
            title: '操作',
            dataIndex: 'operation',
            key: 'operation',
            render: (text, record, index) => {
                return (
                    <div>
                        <Button type="Default" style={{marginRight: 10}} onClick={() => this.view(record)}>查看</Button>
                        <Button type="Default" style={{marginRight: 10}} onClick={() => this.edit(record)}>编辑</Button>
                        {record.publish && <Button type="danger" style={{marginRight: 10}} onClick={() => this.publish(record)}>取消发布</Button>}
                        {!record.publish && <Button type="primary" style={{marginRight: 10}} onClick={() => this.publish(record)}>发布</Button>}
                        <Button type="danger" onClick={() => this.del(record)}>删除</Button>
                    </div>
                )
            },
        }];
        return (
            <div>
                <Row gutter={24}>
                    <Col className="gutter-row" span={8}>
                        <Button type="primary" style={{marginRight: 10}} onClick={this.add}>
                            添加广告
                        </Button>
                    </Col>
                    <Col className="gutter-row" span={8} offset={8}>
                        <Search
                            placeholder="请输入广告名"
                            onSearch={value => {
                                this.setState({
                                    filterType: null
                                });
                                if (value !== "") {
                                    this.fetch({
                                        keyword: value
                                    })
                                } else {
                                    this.fetch()
                                }
                            }}
                            style={{width: 200, marginRight: 10, position: 'absolute', right: 0}}
                        />
                    </Col>
                </Row>

                <Table style={{marginTop: 10}}
                       columns={columns}
                       rowKey={record => record.id}
                       dataSource={this.state.data}
                       size="small"
                       pagination={this.state.pagination}
                       loading={this.state.loading}
                       onChange={this.handleTableChange}
                />
                <AdsModal
                    visible={addVisible}
                    fields={this.state.fields}
                    parent={this}
                    confirmLoading={confirmLoading}
                    onCancel={() => {
                        this.setState({
                            addVisible: false,
                        });
                    }}
                />
                <AdsViewModal
                    visible={viewVisible}
                    fields={this.state.fields}
                    onCancel={() => {
                        this.setState({
                            viewVisible: false,
                        });
                    }}
                />
            </div>
        );
    }
}

export default Ads;
