import {Component} from "react";
import {message, Modal, Select, Form, Input, InputNumber} from "antd";
import React from "react";
import fetch from '../../util/fetch'

const {Option} = Select;

export default Form.create({name: 'add_ads'})(
    class AdsModal extends Component {

        constructor(...props) {
            super(...props);
            this.submit = this.submit.bind(this)
        }

        state = {
            confirmLoading: false
        };

        submit = function () {
            const form = this.props.form;
            const {
                onCancel
            } = this.props;
            form.validateFields((err, values) => {
                if (err) {
                    return;
                }
                this.setState({
                    confirmLoading: true
                });
                let url = this.props.fields.from === 'add' ? '/api/ads/add' : '/api/ads/update';
                fetch(url, {
                    method: 'post',
                    body: JSON.stringify({
                        id: this.props.fields.id,
                        publish: this.props.fields.publish,
                        ...form.getFieldsValue()
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(response => {
                    this.setState({
                        confirmLoading: false
                    });
                    form.resetFields();
                    onCancel();
                    message.success('成功');
                    this.props.parent.fetch({
                        limit: 10,
                        offset: 0
                    })
                }).catch(err => {
                    message.error('失败');
                });
            });
        };

        render() {
            const {
                visible, onCancel
            } = this.props;

            const formItemLayout = {
                labelCol: {span: 6},
                wrapperCol: {span: 14},
            };
            const {getFieldDecorator, getFieldValue} = this.props.form;

            return (
                <Modal
                    title="添加新闻"
                    visible={visible}
                    confirmLoading={this.state.confirmLoading}
                    cancelText="取消"
                    onCancel={onCancel}
                    okText={this.props.fields.from === 'add' ? '添加' : '保存'}
                    onOk={this.submit}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="广告名称"
                        >
                            {getFieldDecorator('name', {
                                rules: [
                                    {required: true, message: '请输入广告名称!'},
                                ],
                                initialValue: this.props.fields.name || ''
                            })(
                                <Input placeholder="请输入广告名称"/>
                            )}

                        </Form.Item>
                        <Form.Item
                            label="广告链接"
                        >
                            {getFieldDecorator('link', {
                                rules: [
                                    {required: true, message: '请输入广告链接!'},
                                ],
                                initialValue: this.props.fields.link || ''
                            })(
                                <Input placeholder="请输入广告链接"/>
                            )}
                        </Form.Item>
                        {
                            <Form.Item
                                label="新闻类型"
                                hasFeedback
                            >
                                {getFieldDecorator('type', {
                                    rules: [
                                        {required: true, message: '请选择新闻类型!'},
                                    ],
                                    initialValue: this.props.fields.type || ''
                                })(
                                    <Select placeholder="请选择新闻类型">
                                        <Option value="banner">banner</Option>
                                        <Option value="hotWord">热词</Option>
                                    </Select>
                                )}
                            </Form.Item>
                        }

                        {/*<Form.Item*/}
                        {/*label="上传图片"*/}
                        {/*extra="请上传图片"*/}
                        {/*style={{display: getFieldValue('type') === 'banner' ? 'block' : 'none'}}*/}
                        {/*>*/}
                        {/*{getFieldDecorator('imgLink', {*/}
                        {/*valuePropName: 'fileList',*/}
                        {/*getValueFromEvent: this.normFile,*/}
                        {/*})(*/}
                        {/*<Upload name="logo" action="/upload.do" listType="picture">*/}
                        {/*<Button>*/}
                        {/*<Icon type="upload"/> 上传广告图片*/}
                        {/*</Button>*/}
                        {/*</Upload>*/}
                        {/*)}*/}
                        {/*</Form.Item>*/}

                        {getFieldValue('type') === 'banner' &&
                        <Form.Item label="banner位">
                            {getFieldDecorator('order', {
                                rules: [
                                    {required: true, message: '请选择banner位置!'},
                                ],
                                initialValue: this.props.fields.order || ''
                            })(
                                <Select placeholder="请选择banner位">
                                    {[1,2,3,4,5].map(i => <Option value={`${i}`}>{i}</Option>)}
                                </Select>
                            )}
                        </Form.Item>}

                        {getFieldValue('type') === 'banner' &&
                        <Form.Item label="图片链接">
                            {getFieldDecorator('imgLink', {
                                rules: [
                                    {required: true, message: '请输入banner图片链接!'},
                                ],
                                initialValue: this.props.fields.imgLink || ''
                            })(
                                <Input placeholder="请输入banner图片链接"/>
                            )}
                        </Form.Item>}

                        {getFieldValue('type') === 'hotWord' &&
                        <Form.Item label="热词权重">
                            {getFieldDecorator('weight', {
                                valuePropName: 'weight',
                                initialValue: this.props.fields.weight || 50
                            })(
                                <InputNumber
                                    value={this.props.fields.weight || 50}
                                    min={0}
                                    max={100}
                                    formatter={value => `${value}%`}
                                />
                            )}
                        </Form.Item>}

                    </Form>
                </Modal>
            )
        }
    }
);