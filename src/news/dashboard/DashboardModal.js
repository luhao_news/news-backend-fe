import {Component} from "react";
import moment from 'moment';
import {message, Modal, Form, Input, Select, InputNumber, DatePicker} from "antd";
import React from "react";
import fetch from '../../util/fetch'

const {Option} = Select;

export default Form.create({name: 'add_dashboard'})(
    class UserModal extends Component {

        constructor(...props) {
            super(...props);
            this.submit = this.submit.bind(this)
        }

        componentDidMount() {
            fetch('/api/user/getAllUsers', {
                method: 'post',
            }).then(res => res.json()).then(data => {
                console.log(data);
                this.setState({
                    userList: data.data,
                });
            }).catch(err => {
                message.error('获取用户列表失败');
            });
        }

        state = {
            userList: [],
            confirmLoading: false
        };

        submit = function () {
            const form = this.props.form;
            const {
                onCancel
            } = this.props;
            form.validateFields((err, values) => {
                if (err) {
                    return;
                }
                this.setState({
                    confirmLoading: true
                });
                let url = this.props.fields.from === 'add' ? '/api/dashboard/add' : '/api/dashboard/update';
                fetch(url, {
                    method: 'post',
                    body: JSON.stringify({
                        id: this.props.fields.id,
                        ...form.getFieldsValue()
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(response => {
                    this.setState({
                        confirmLoading: false
                    });
                    form.resetFields();
                    onCancel();
                    message.success('成功');
                    this.props.parent.fetch({
                        limit: 10,
                        offset: 0
                    })
                }).catch(err => {
                    message.error('失败');
                });
            });
        };

        render() {
            const {
                visible, onCancel
            } = this.props;

            const formItemLayout = {
                labelCol: {span: 6},
                wrapperCol: {span: 14},
            };
            const {getFieldDecorator} = this.props.form;
            let user = this.state.userList.find(u => u.id === this.props.fields.userId)
            let userName;
            if (user) {
                userName = user.userName
            }
            return (
                <Modal
                    title="添加记录"
                    visible={visible}
                    confirmLoading={this.state.confirmLoading}
                    cancelText="取消"
                    onCancel={onCancel}
                    okText={this.props.fields.from === 'add' ? '添加' : '保存'}
                    onOk={this.submit}
                >
                    <Form {...formItemLayout}>

                        <Form.Item
                          label="日期"
                        >
                          {getFieldDecorator('createTime', {
                            rules: [
                              {required: true, message: '请选择日期!'},
                            ],
                            initialValue: moment(this.props.fields.createTime) || moment()
                          })(
                            <DatePicker format="YYYY-MM-DD" />
                          )}
                        </Form.Item>

                        <Form.Item
                          label="用户"
                        >
                            {getFieldDecorator('userId', {
                                rules: [
                                    {required: true, message: '请选择用户!'},
                                ],
                                initialValue: this.props.fields.userId || ''
                            })(
                              <Select placeholder="请选择用户">
                                  {this.state.userList.map(user => {
                                      return <Option value={user.id}>{user.userName}</Option>
                                  })}
                              </Select>
                            )}
                        </Form.Item>

                        <Form.Item
                            label="渠道名称"
                        >
                            {getFieldDecorator('channelName', {
                                rules: [
                                    {required: true, message: '请输入渠道名称!'},
                                ],
                                initialValue: this.props.fields.channelName || ''
                            })(
                                <Input placeholder="请输入渠道名称"/>
                            )}
                        </Form.Item>
                        <Form.Item
                          label="渠道号"
                        >
                            {getFieldDecorator('channelCode', {
                                rules: [
                                    {required: true, message: '请输入渠道号!'},
                                ],
                                initialValue: this.props.fields.channelCode || ''
                            })(
                              <Input placeholder="请输入渠道号"/>
                            )}
                        </Form.Item>
                        <Form.Item
                          label="点击量"
                        >
                            {getFieldDecorator('uv', {
                                rules: [
                                    {required: true, message: '请输入点击量'},
                                ],
                                initialValue: this.props.fields.uv || ''
                            })(
                              <InputNumber placeholder="请输入点击量"/>
                            )}
                        </Form.Item>
                        <Form.Item
                          label="结算收入"
                        >
                            {getFieldDecorator('earning', {
                                rules: [
                                    {required: true, message: '请输入结算收入!'},
                                ],
                                initialValue: this.props.fields.earning || ''
                            })(
                              <InputNumber placeholder="请输入结算收入"/>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
    }
);