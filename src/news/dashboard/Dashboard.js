import moment from "moment";

import React, {Component} from 'react';
import Cookies from "js-cookie";

import 'antd/dist/antd.css';
import fetch from "../../util/fetch";
import {Button, Col, Input, message, Row, Table, DatePicker, Select, Modal} from "antd";

import DashboardModal from "../dashboard/DashboardModal";

const Search = Input.Search;

const {Option} = Select;

let {RangePicker} = DatePicker;


const confirm = Modal.confirm;

class Dashboard extends Component {
    state = {
        data: [],
        pagination: {},
        loading: false,
        addVisible: false,
        viewVisible: false,
        confirmLoading: false,
        keyword: "",
        type: null,
        sort: null,
        filters: null,
        fields: {},
        userList: []
    };

    componentDidMount() {
        this.fetch({
            limit: 10,
            offset: 0
        });
        fetch('/api/user/getAllUsers', {
            method: 'post',
        }).then(res => res.json()).then(data => {
            console.log(data);
            this.setState({
                userList: data.data,
            });
        }).catch(err => {
            message.error('获取用户列表失败');
        });
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.offset = (pagination.current - 1) * pagination.pageSize;
        this.setState({
            pagination: pager,
            filters,
            sorter,
        });

        if (filters.type) {
            filters.type = filters.type[0]
        }
        let query = {
            limit: pager.pageSize,
            offset: pager.offset,
            sort: (sorter.field && sorter.order) ? `${sorter.field}:${sorter.order}` : null,
            ...filters,
        };
        this.fetch(query);
    };

    fetch = (params = {}) => {
        this.setState({loading: true});
        let {startTime, endTime, userId} = this.state;
        params = {...params, startTime, endTime, userId};
        fetch('/api/dashboard/list', {
            method: 'post',
            body: JSON.stringify(params),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            const pagination = {...this.state.pagination};
            pagination.total = data.data.count;
            this.setState({
                loading: false,
                data: data.data.result,
                pagination,
            });
        }).catch(err => {
            message.error('获取数据失败');
        });
    };


    add = () => {
        this.setState({
            fields: {
                from: 'add'
            },
            addVisible: true
        });
    };

    edit = (value) => {
        this.setState({
            fields: {...value, from: 'edit'},
            addVisible: true,
        });
    };

    del = (value) => {
        let that = this;
        confirm({
            title: '确认删除该条数据？',
            cancelText: '取消',
            okText: '确定',
            onOk() {
                var data = new URLSearchParams();
                data.append('id', value.id);
                fetch('/api/dashboard/delete', {
                    method: 'post',
                    body: data.toString(),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                }).then(response => {
                    message.success('删除成功');
                    that.fetch({
                        limit: 10,
                        offset: 0
                    });
                }).catch(err => {
                    message.error('删除失败');
                });
            },
            onCancel() {},
        });

    };


    render() {
        let {addVisible, confirmLoading} = this.state;

        const columns = [{
            title: '日期',
            dataIndex: 'createTime',
            sorter: true,
            sortDirections: ['asc', 'desc'],
            width: '20%',
            render: (value) => {
                return moment(value).format('YYYY-MM-DD')
            }
        },{
            title: '用户',
            dataIndex: 'userId',
            render: (value) => {
                let user = this.state.userList.find(u => u.id === value);
                return user ? user.userName : '该用户已不再系统中'
            }
        },{
            title: '渠道名称',
            dataIndex: 'channelName',
        }, {
            title: '渠道号',
            dataIndex: 'channelCode',
        }, {
            title: '点击量',
            dataIndex: 'uv',
        },  {
            title: '结算收入',
            dataIndex: 'earning',
        }];
        if (Cookies.get('user_type') === 'admin') {
            columns.push({
                title: '操作',
                dataIndex: 'operation',
                key: 'operation',
                render: (text, record, index) => {
                    return (
                      <div>
                          <Button type="Default" style={{marginRight: 10}} onClick={() => this.edit(record)}>编辑</Button>
                          <Button type="danger" onClick={() => this.del(record)}>删除</Button>
                      </div>
                    )
                },
            })
        }
        return (
          <div>
              <Row gutter={24}>
                  <Col className="gutter-row" span={6}>
                      <RangePicker
                        format="YYYY-MM-DD"
                        onChange={(_, [startTime, endTime]) =>{
                            this.setState({
                                startTime: startTime === '' ? null : startTime,
                                endTime: endTime === '' ? null : endTime
                            }, () => {
                                this.fetch({
                                    limit: 10,
                                    offset: 0
                                });
                            });
                        }}
                      />
                  </Col>
                  {Cookies.get('user_type') === 'admin' &&
                      <Col className="gutter-row" span={4}>
                          <Button type="primary" style={{marginRight: 10}} onClick={this.add}>
                              添加记录
                          </Button>
                      </Col>
                  }

                  <Col className="gutter-row" span={6}>
                      <Search
                        placeholder="请输入渠道号"
                        onSearch={value => {
                            if (value !== "") {
                                this.fetch({
                                    keyword: value
                                })
                            } else {
                                this.fetch({
                                    limit: 10,
                                    offset: 0
                                });
                            }
                        }}
                        style={{width: 200, marginRight: 10, position: 'absolute', right: 0}}
                      />
                  </Col>
                  {Cookies.get('user_type') === 'admin' &&
                      <Col className="gutter-row" span={6}>
                          <Select allowClear={true} style={{width: 120}} placeholder="请选择用户" onChange={(value) => {
                              this.setState({userId: value === '' ? null : value}, () => {
                                  this.fetch({
                                      limit: 10,
                                      offset: 0
                                  });
                              })
                          }}>
                              {this.state.userList.map(user => {
                                  return <Option value={user.id}>{user.userName}</Option>
                              })}
                          </Select>
                      </Col>
                  }
              </Row>

              <Table style={{marginTop: 10}}
                     columns={columns}
                     rowKey={record => record.id}
                     dataSource={this.state.data}
                     size="small"
                     pagination={this.state.pagination}
                     loading={this.state.loading}
                     onChange={this.handleTableChange}
              />
              <DashboardModal
                visible={addVisible}
                fields={this.state.fields}
                parent={this}
                confirmLoading={confirmLoading}
                onCancel={() => {
                    this.setState({
                        addVisible: false,
                    });
                }}
              />
          </div>
        );
    }
}

export default Dashboard;
