import React, {Component} from 'react';

import {Icon, Layout, Menu} from 'antd';
import Cookies from 'js-cookie';
import {BrowserRouter as Router, NavLink, Redirect, Route, Switch} from "react-router-dom";
import Dashboard from './dashboard/Dashboard'
import Ads from './ads/Ads'
import User from './user/User'
import fetch from '../util/fetch'

const {Header, Sider, Content} = Layout;


class Main extends Component {
    state = {
        collapsed: false,
        userType: Cookies.get('user_type'),
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    logout = () => {
        fetch('/api/logout', {
            method: "GET"
        }).then(res => {
            if (res.status === 200) {
                this.props.history.push('/login')
            }
        })
    };

    render() {
        let key = window.location.pathname.replace(/\//g, '');
        console.log('userType: ', this.state.userType);
        console.log('userType: ', Cookies.get('user_type'));
        return (
            <Router>
                <Layout style={{height: 'auto'}}>
                    <Sider
                        trigger={null}
                        collapsible
                        collapsed={this.state.collapsed}
                        style={{height: "100vh"}}
                    >
                        <div className="logo"
                             style={{textAlign: this.state.collapsed ? 'center' : 'left'}}>
                            {this.state.collapsed ?
                                <Icon type="bank"/> :
                                <div><span>管理系统后台</span></div>
                            }
                        </div>
                        <div className="logout" hidden={this.state.collapsed} onClick={this.logout}>
                            <Icon type="logout"/><span>登出</span>
                        </div>
                        <Menu theme="dark" mode="inline" defaultSelectedKeys={[key]}>
                            {this.state.userType === 'admin' &&
                                <Menu.Item key="user">
                                    <NavLink to="/user">
                                        <Icon type="user"/>
                                        <span>用户管理</span>
                                    </NavLink>
                                </Menu.Item>
                            }
                            {this.state.userType === 'admin' &&
                                <Menu.Item key="ads">
                                    <NavLink to="/ads">
                                        <Icon type="video-camera"/>
                                        <span>广告管理</span>
                                    </NavLink>
                                </Menu.Item>
                            }

                            <Menu.Item key="dashboard">
                                <NavLink to="/dashboard">
                                    <Icon type="eye"/>
                                    <span>渠道返量数据</span>
                                </NavLink>
                            </Menu.Item>

                        </Menu>
                    </Sider>
                    <Layout>
                        <Header style={{background: '#fff', padding: 0}}>
                            <Icon
                                className="trigger"
                                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                onClick={this.toggle}
                            />
                        </Header>
                        <Content style={{
                            margin: '24px 16px', padding: 24, background: '#fff'
                        }}
                        >
                            <Switch>
                                <Route path="/dashboard" component={Dashboard}/>
                                <Route path="/user" component={User}/>
                                <Route path="/ads" component={Ads}/>
                                <Route path="*">
                                    <Redirect from="/" to={this.state.userType === 'admin' ? "/ads": "/dashboard"}/>
                                </Route>
                            </Switch>
                        </Content>
                    </Layout>
                </Layout>
            </Router>
        );
    }
}

export default Main;

