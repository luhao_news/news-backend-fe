import {Component} from "react";
import {message, Modal, Form, Input} from "antd";
import React from "react";
import fetch from '../../util/fetch'


export default Form.create({name: 'add_user'})(
    class UserModal extends Component {

        constructor(...props) {
            super(...props);
            this.submit = this.submit.bind(this)
        }

        state = {
            confirmLoading: false
        };

        submit = function () {
            const form = this.props.form;
            const {
                onCancel
            } = this.props;
            form.validateFields((err, values) => {
                if (err) {
                    return;
                }
                this.setState({
                    confirmLoading: true
                });
                let url = this.props.fields.from === 'add' ? '/api/user/add' : '/api/user/update';
                fetch(url, {
                    method: 'post',
                    body: JSON.stringify({
                        id: this.props.fields.id,
                        ...form.getFieldsValue()
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(response => {
                    this.setState({
                        confirmLoading: false
                    });
                    form.resetFields();
                    onCancel();
                    message.success('成功');
                    this.props.parent.fetch({
                        limit: 10,
                        offset: 0
                    })
                }).catch(err => {
                    message.error('失败');
                });
            });
        };

        render() {
            const {
                visible, onCancel
            } = this.props;

            const formItemLayout = {
                labelCol: {span: 6},
                wrapperCol: {span: 14},
            };
            const {getFieldDecorator} = this.props.form;

            return (
                <Modal
                    title="添加用户"
                    visible={visible}
                    confirmLoading={this.state.confirmLoading}
                    cancelText="取消"
                    onCancel={onCancel}
                    okText={this.props.fields.from === 'add' ? '添加' : '保存'}
                    onOk={this.submit}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="用户名"
                        >
                            {getFieldDecorator('userName', {
                                rules: [
                                    {required: true, message: '请输入用户名!'},
                                ],
                                initialValue: this.props.fields.userName || ''
                            })(
                                <Input placeholder="请输入用户名"/>
                            )}

                        </Form.Item>
                        <Form.Item
                            label="密码"
                        >
                            {getFieldDecorator('password', {
                                rules: [
                                    {required: true, message: '请输入密码!'},
                                ],
                                initialValue: this.props.fields.password || ''
                            })(
                                <Input placeholder="请输入密码"/>
                            )}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
    }
);