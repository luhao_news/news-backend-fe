import {Component} from "react";
import {Modal, Form} from "antd";
import React from "react";

export default Form.create({name: 'view_user'})(
    class UserViewModal extends Component {
        state = {

        };

        init = () => {
            let fields = this.props.fields;
            const {setFieldsValue} = this.props.form;
            setFieldsValue('userName', fields.userName)
        };

        render() {
            const {
                visible, onCancel
            } = this.props;

            const formItemLayout = {
                labelCol: {span: 6},
                wrapperCol: {span: 14},
            };
            return (
                <Modal
                    title="查看用户"
                    visible={visible}
                    okText="确定"
                    cancelButtonProps={{style: {display: 'none'}}}
                    onCancel={onCancel}
                    onOk={onCancel}
                >
                    <Form {...formItemLayout}>
                        <Form.Item
                            label="用户名"
                        >
                            <span className="ant-form-text">{this.props.fields.userName}</span>
                        </Form.Item>
                        <Form.Item
                          label="密码"
                        >
                            <span className="ant-form-text">{this.props.fields.password}</span>
                        </Form.Item>

                    </Form>
                </Modal>
            )
        }
    }
);
