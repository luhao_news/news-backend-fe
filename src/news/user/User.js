import React, {Component} from 'react';
import moment from 'moment'
import fetch from "../../util/fetch";
import {Button, Col, Input, message, Row, Table, Modal} from "antd";
import UserModal from "../user/UserModal";
import UserViewModal from "../user/UserViewModal";
import Cookies from "js-cookie";

const Search = Input.Search;
const confirm = Modal.confirm;

class User extends Component {
    state = {
        data: [],
        pagination: {},
        loading: false,
        addVisible: false,
        viewVisible: false,
        confirmLoading: false,
        keyword: "",
        type: null,
        sort: null,
        filters: null,
        fields: {}
    };

    componentDidMount() {
        if (Cookies.get('user_type') !== 'admin') {
            this.props.history.push('/dashboard')
        } else {
            this.fetch({
                limit: 10,
                offset: 0
            });
        }
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = {...this.state.pagination};
        pager.offset = (pagination.current - 1) * pagination.pageSize;
        this.setState({
            pagination: pager,
            filters,
            sorter,
        });

        if (filters.type) {
            filters.type = filters.type[0]
        }
        let query = {
            limit: pager.pageSize,
            offset: pager.offset,
            sort: (sorter.field && sorter.order) ? `${sorter.field}:${sorter.order}` : null,
            ...filters,
        };
        this.fetch(query);
    };

    fetch = (params = {}) => {
        this.setState({loading: true});

        fetch('/api/user/list', {
            method: 'post',
            body: JSON.stringify(params),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            const pagination = {...this.state.pagination};
            pagination.total = data.data.count;
            this.setState({
                loading: false,
                data: data.data.result,
                pagination,
            });
        }).catch(err => {
            message.error('获取数据失败');
        });
    };

    add = () => {
        this.setState({
            fields: {
                from: 'add'
            },
            addVisible: true
        });
    };

    edit = (value) => {
        this.setState({
            fields: {...value, from: 'edit'},
            addVisible: true,
        });
    };

    view = (value) => {
        this.setState({
            fields: {...value},
            viewVisible: true,
        });
    };

    del = (value) => {
        let that = this;
        confirm({
            title: '确认删除该用户?',
            cancelText: '取消',
            okText: '确定',
            onOk() {
                var data = new URLSearchParams();
                data.append('id', value.id);
                fetch('/api/user/delete', {
                    method: 'post',
                    body: data.toString(),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    },
                }).then(response => {
                    message.success('删除成功');
                    that.fetch({
                        limit: 10,
                        offset: 0
                    });
                }).catch(err => {
                    message.error('删除失败');
                });
            },
            onCancel() {},
        });
    };


    render() {
        let {addVisible, viewVisible, confirmLoading} = this.state;

        const columns = [{
            title: '添加日期',
            dataIndex: 'createTime',
            sorter: true,
            sortDirections: ['asc', 'desc'],
            width: '20%',
            render: (value) => {
                return moment(value).format('YYYY-MM-DD HH:mm:ss')
            }
        }, {
            title: '用户名',
            dataIndex: 'userName',
        }, {
            title: '操作',
            dataIndex: 'operation',
            key: 'operation',
            render: (text, record, index) => {
                return (
                  <div>
                      <Button type="Default" style={{marginRight: 10}} onClick={() => this.view(record)}>查看</Button>
                      <Button type="Default" style={{marginRight: 10}} onClick={() => this.edit(record)}>编辑</Button>
                      <Button type="danger" onClick={() => this.del(record)}>删除</Button>
                  </div>
                )
            },
        }];
        return (
          <div>
              <Row gutter={24}>
                  <Col className="gutter-row" span={8}>
                      <Button type="primary" style={{marginRight: 10}} onClick={this.add}>
                          添加用户
                      </Button>
                  </Col>
                  <Col className="gutter-row" span={8} offset={8}>
                      <Search
                        placeholder="请输入用户名"
                        onSearch={value => {
                            if (value !== "") {
                                this.fetch({
                                    keyword: value
                                })
                            } else {
                                this.fetch()
                            }
                        }}
                        style={{width: 200, marginRight: 10, position: 'absolute', right: 0}}
                      />
                  </Col>
              </Row>

              <Table style={{marginTop: 10}}
                     columns={columns}
                     rowKey={record => record.id}
                     dataSource={this.state.data}
                     size="small"
                     pagination={this.state.pagination}
                     loading={this.state.loading}
                     onChange={this.handleTableChange}
              />
              <UserModal
                visible={addVisible}
                fields={this.state.fields}
                parent={this}
                confirmLoading={confirmLoading}
                onCancel={() => {
                    this.setState({
                        addVisible: false,
                    });
                }}
              />
              <UserViewModal
                visible={viewVisible}
                fields={this.state.fields}
                onCancel={() => {
                    this.setState({
                        viewVisible: false,
                    });
                }}
              />
          </div>
        );
    }
}

export default User;
